package com.phattharasi.crystal.gotogeter;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by phatt on 4/7/2559.
 */
public class DbHelper extends SQLiteOpenHelper {
    private static final String databaseName = "gotogeter.sqlite";
    private static final int databaseVersion = 1;
    private Context myContext;

    public DbHelper(Context context) {
        super(context, databaseName, null, databaseVersion);
        this.myContext = context;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        //  ตารางภูมิภาค
        String SQLText = "CREATE TABLE region(" +
                "id_region TEXT PRIMARY KEY," +
                "name_region TEXT);";
        db.execSQL(SQLText);

//        ตารางเก็บข้อมูลสถานที่
        SQLText = "CREATE TABLE knowledge(" +
                "id_know TEXT PRIMARY KEY," +
                "name_know TEXT," +
                "id_region TEXT" +
                "name_region TEXT," +
                "img TEXT," +
                "content TEXT);";
        db.execSQL(SQLText);

//        ใส่ข้อมูลสถานที่
        SQLText = "INSERT INTO product (id_know, name_know, county, id_region, name_region, img, content)" +
                "VALUES('K001', 'พระบรมธาตุดอยสุเทพ', 'จ.เชียงใหม่', 'R1', 'ภาคเหนือ', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis tempor ligula fermentum dolor ultrices consectetur. ');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO product (id_know, name_know, county, id_region, name_region, img, content)" +
                "VALUES('K002', 'ดอยแม่สลอง', 'จ.เชียงราย', 'R1', 'ภาคเหนือ', '', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis tempor ligula fermentum dolor ultrices consectetur. ');";
        db.execSQL(SQLText);

//        ข้อมูลภูมิภาค
        SQLText = "INSERT INTO type (type_id, name_type)" +
                "VALUES('R1', 'ภาคเหนือ');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO type (type_id, name_type)" +
                "VALUES('R2', 'ภาคกลาง');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO type (type_id, name_type)" +
                "VALUES('R3', 'ภาคอีสาน');";
        db.execSQL(SQLText);
        SQLText = "INSERT INTO type (type_id, name_type)" +
                "VALUES('R4', 'ภาคใต้');";
        db.execSQL(SQLText);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
