package com.phattharasi.crystal.gotogeter;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

public class KnowActivity extends AppCompatActivity {
    private TextView nameknow, county, content;
    private ImageView img;
    private ScrollView scrollView;
    private Knowledge knowledge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_know);

        Intent intent = getIntent();
        knowledge = (Knowledge) intent.getSerializableExtra("knowObj");

        scrollView = (ScrollView) findViewById(R.id.scrollView);

        nameknow = (TextView) findViewById(R.id.name_know);
        county = (TextView) findViewById(R.id.county);
        content = (TextView) findViewById(R.id.county);

        nameknow.setText(knowledge.getNameKnow());
        county.setText(knowledge.getCounty());
        content.setText(knowledge.getContent());

        img = (ImageView) findViewById(R.id.img);
    }
}
