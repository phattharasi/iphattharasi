package com.phattharasi.crystal.gotogeter;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by phatt on 6/7/2559.
 */
public class KnowledgeDAO {
    private SQLiteDatabase database;
    private DbHelper dbHelper;

    public KnowledgeDAO(Context context) {
        dbHelper = new DbHelper(context);
        database = dbHelper.getWritableDatabase();
    }

    public void close(){
        database.close();
    }

    public ArrayList<Knowledge>findAll(){
        ArrayList<Knowledge> knowledges = new ArrayList<>();
        String sqlText = "SELECT * FROM product ORDER BY id_know DESC;";
        Cursor cursor = database.rawQuery(sqlText, null);
        cursor.moveToFirst();
        Knowledge knowledge;
        while (!cursor.isAfterLast()){
            knowledge = new Knowledge();
            knowledge.setIdKnow(cursor.getString(0));
            knowledge.setNameKnow(cursor.getString(1));
            knowledge.setCounty(cursor.getString(2));
            knowledge.setIdRegion(cursor.getString(3));
            knowledge.setNameRegion(cursor.getString(4));
            knowledge.setImg(cursor.getString(5));
            knowledge.setContent(cursor.getString(6));

            knowledges.add(knowledge);

            cursor.moveToNext();
        }
        return knowledges;
    }
}
