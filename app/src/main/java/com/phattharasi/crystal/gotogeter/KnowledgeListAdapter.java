package com.phattharasi.crystal.gotogeter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by phatt on 6/7/2559.
 */
public class KnowledgeListAdapter extends BaseAdapter {
    private static Activity activity;
    private static LayoutInflater layoutInflater;
    private ArrayList<Knowledge> knowledges;

    public KnowledgeListAdapter(Activity activity, ArrayList<Knowledge> knowledges) {
        this.activity = activity;
        this.knowledges = knowledges;
        this.layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return knowledges.size();
    }

    @Override
    public Object getItem(int i) {
        return this.knowledges.get(i);
    }

    @Override
    public long getItemId(int i) {
        return Long.parseLong(this.knowledges.get(i).getIdKnow());
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View view1 = view;
        view1 = layoutInflater.inflate(R.layout.listviwe_know, null);
        TextView nameKnow = (TextView) view.findViewById(R.id.name_know);

        nameKnow.setText(this.knowledges.get(i).getNameKnow());

        return view1;
    }
}
